package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieTrailerDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper {

    public Movie mapToEntity(CreateMovieDto createMovieDto) {
        Movie movie = new Movie();

        movie.setImage(createMovieDto.getImage());
        movie.setTitle(createMovieDto.getTitle());
        movie.setYear(createMovieDto.getYear());
        movie.setVideoId(createMovieDto.getVideoId());

        return movie;
    }

    public MovieTrailerDto movieToTrailerDto(Movie movie){
        MovieTrailerDto movieTrailerDto = new MovieTrailerDto();

        movieTrailerDto.setTitle(movie.getTitle());
        movieTrailerDto.setVideoId(movie.getVideoId());

        return movieTrailerDto;
    }

}
